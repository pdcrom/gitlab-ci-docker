import os
import re
import sys
import semver
import gitlab

def verify_env_var_presence(name):
    if name not in os.environ:
        raise Exception(f"Expected the following environment variable to be set: {name}")


def get_commit_tag(project, commit_id):

    # get tag for commit 'commit_is' in project 'project'
    commit_tag=None

    #get projects tags
    tags = project.tags.list()
    for tag in tags:
        id=tag.commit['id']
        #print("%s %s"%(tag.name,id))
        if id == commit_id:
            commit_tag=tag.name
            break

    return commit_tag


def get_commit_labels(project,commit):

    message=commit.message

    #get merge-id
    matches = re.search(r'(\S*\/\S*!)(\d+)', message, re.M|re.I)

    if matches == None:
        #Exception(f"Unable to extract merge request from commit message: {message}")
        print(f"Unable to extract merge request from commit message: {message}")
        labels=[]
    else:    
        merge_request_id=matches.group(2)  
        merge_request = project.mergerequests.get(merge_request_id)
        labels=merge_request.labels
        print(f'Using labels from merge request: id={merge_request_id}, Title={merge_request.title}')
        print(f'Labels: {labels}')

    return labels


def get_latest_tag(project):

    latest = None
    tags = project.tags.list()
    if tags:
        tag=tags[0]
        latest=tag.name

    return latest


def bump_version(project,commit,latest):

    labels = get_commit_labels(project,commit)

    #bump version
    if "bump-minor" in labels:
        return semver.bump_minor(latest)
    elif "bump-major" in labels:
        return semver.bump_major(latest)
    else:
        return semver.bump_patch(latest)


#########################################
# MAIN
def main():
    env_list = ["CI_SERVER_URL", "GITLAB_TOKEN", "CI_PROJECT_ID", "CI_COMMIT_SHA"]
    [verify_env_var_presence(e) for e in env_list]

    # get environment variables
    GITLAB_URL=os.environ['CI_SERVER_URL']
    GITLAB_TOKEN=os.environ['GITLAB_TOKEN']
    CI_PROJECT_ID=os.environ['CI_PROJECT_ID']
    CI_COMMIT_SHA=os.environ['CI_COMMIT_SHA'] 

    # gitlab login
    gl = gitlab.Gitlab(GITLAB_URL,private_token=GITLAB_TOKEN)
    gl.auth()

    project=gl.projects.get(CI_PROJECT_ID)
    commit=project.commits.get(CI_COMMIT_SHA)

    print(f'Tag commit {CI_COMMIT_SHA} for project {project.name}')

    # if commit is already tagged, exit
    commit_tag=get_commit_tag(project, CI_COMMIT_SHA)
    # if commit_tag:
    #      raise Exception(f"Current commit already has tag: {commit_tag}")

    latest = get_latest_tag(project)
    if latest:
        version=bump_version(project,commit,latest)
    else:
        # No tag found in project
        latest='-'
        version='1.0.0'

    print('latest tag=',latest)
    print('new tag=',version)

    #create tag
    tag=project.tags.create({'tag_name': version, 'ref': CI_COMMIT_SHA})

    #write version to file
    with open('version.txt', 'w+') as f:  
        f.write(version)

# main
main()

