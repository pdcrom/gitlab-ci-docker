FROM docker:latest

COPY requirements.txt .

RUN apk add --no-cache python3 && \
    pip3 install --upgrade pip && \
    pip3 install -r requirements.txt

WORKDIR /version-update

COPY version-update.py .
